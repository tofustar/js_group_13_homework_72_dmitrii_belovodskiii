import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesComponent } from './recipes/recipes.component';
import { EditRecipeComponent } from './edit-recipe/edit-recipe.component';
import { RecipeDetailsComponent } from './recipes/recipe-details/recipe-details.component';
import { RecipeResolverService } from './recipes/recipe-resolver.service';

const routes: Routes = [
  {path: '', component: RecipesComponent},
  {path: 'edit-recipe', component: EditRecipeComponent},
  {path: ':id', component: RecipeDetailsComponent, resolve: {
    recipe: RecipeResolverService
    }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
