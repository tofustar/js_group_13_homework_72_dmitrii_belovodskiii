import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from './recipe.model';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()

export class RecipeService {
  recipesChange = new Subject<Recipe[]>();
  private recipes: Recipe[] = [];

  constructor(private http: HttpClient) {}

  fetchRecipes() {
    return this.http.get<{[key:string]: Recipe}>('https://js-group-13-default-rtdb.firebaseio.com/recipes.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const recipeData = result[id];
          console.log(recipeData);
          return new Recipe(id, recipeData.title, recipeData.description, recipeData.img, recipeData.ingredients, recipeData.steps)
        });
      }))
      .subscribe(recipes => {
        this.recipes = recipes;
        this.recipesChange.next(this.recipes.slice());
      });
  }

  fetchRecipe(id: string) {
    return this.http.get<Recipe>(`https://js-group-13-default-rtdb.firebaseio.com/recipes/${id}.json`)
      .pipe(map(result => {
        return new Recipe(id, result.title, result.description, result.img, result.ingredients, result.steps)
      }))
  }

  addRecipe(recipe: Recipe) {
    return this.http.post('https://js-group-13-default-rtdb.firebaseio.com/recipes.json', recipe)
  }

  removeRecipe(id: string) {
    return this.http.delete(`https://js-group-13-default-rtdb.firebaseio.com/recipes/${id}.json`)
  }

}
