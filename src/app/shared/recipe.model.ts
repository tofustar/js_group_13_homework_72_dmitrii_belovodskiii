export class Recipe {
  constructor(
    public id: string,
    public title: string,
    public description: string,
    public img: string,
    public ingredients: string,
    public steps: Steps[],
  ) {}
}

class Steps {
  constructor(
    public imgStep: string,
    public descriptionStep: string,
  ) {}

}
