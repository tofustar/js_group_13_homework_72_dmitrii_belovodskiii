import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { RecipeService } from '../shared/recipe.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-recipe',
  templateUrl: './edit-recipe.component.html',
  styleUrls: ['./edit-recipe.component.css']
})
export class EditRecipeComponent implements OnInit {
  recipeForm!: FormGroup;

  constructor(private router: Router, private recipeService: RecipeService) {}

  ngOnInit(): void {
    this.recipeForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      img: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([])
    });
  }

  saveRecipe() {
    this.recipeService.addRecipe(this.recipeForm.value).subscribe(() => {
      void this.router.navigate(['']);
    });
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.recipeForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }

  addStep() {
    const steps = <FormArray>this.recipeForm.get('steps');
    const stepGroup = new FormGroup({
      imgStep: new FormControl('', Validators.required),
      descriptionStep: new FormControl('', Validators.required)
    })
    steps.push(stepGroup);
  }

  getStepControls() {
    const steps = <FormArray>this.recipeForm.get('steps');
    return steps.controls;
  }

}
